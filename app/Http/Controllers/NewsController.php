<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Article;
use App\User;


class NewsController extends Controller {

    public function index() {
      //create a variable that contains all Articles to view.
      $articles = Article::all();

      //pass the Article feeds into the view
      return view('news', [
        'articles' => $articles
      ]);
    }

    public function postArticle()
    {
      $request = request();

      $this->validate($request, [
        'headline' => 'required|max:90|min:15'
      ]);

      $article = new Article;
      $article->headline = $request->input('headline');
      $article->user_id = $request->user()->id;
      $article->save();

      return redirect('/');
    }

    public function getUserArticles($userId) {
      $user = User::find($userId);
      $articles = Article::where('user_id', $userId)->get();

      return view('news', [
        'articles' => $articles
      ]);
    }
}
