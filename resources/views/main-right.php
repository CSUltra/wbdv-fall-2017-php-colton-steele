<div class="main-right-title">Calgary</div>

<div class="weather">
  <div class="flex-container">
    <div class="flex-1 weathertitle">
      <div class="weathertoday"><span class="movetodaydegrees">-5°C</span></div>
      <div>Light Snowfall</div>
    </div>
    <div class="snowflake flex-1">
      <i class="fa fa-snowflake-o fa-5x snowcolor" aria-hidden="true"></i>
    </div>
  </div>
  <div class="flex-container">
    <div class="flex-1 weathertomorrow">
      Wed
      <div>
        <p>-3°C</p><i class="fa fa-cloud fa-2x snowcolor" aria-hidden="true"></i>
      </div>
    </div>
    <div class="flex-1 weathertomorrow">
      Thu
      <div>
        <p>-7°C</p><i class="fa fa-cloud fa-2x snowcolor" aria-hidden="true"></i>
      </div>
    </div>
    <div class="flex-1 weathertomorrow">
      Fri
      <div>
        <p>-4°C</p><i class="fa fa-sun-o fa-2x snowcolor" aria-hidden="true"></i>
      </div>
    </div>


    <div class="flex-1 weathertomorrow">
      Sat
      <div>
        <p>-8°C</p><i class="fa fa-snowflake-o fa-2x snowcolor" aria-hidden="true"></i>
      </div>
    </div>
  </div>
  <div>

    <div class="trending-box">
      <div class="trending-title">Trending</div>

      <div class="trending-tag"><a href="#">Drongald Grump</a></div>
      <div class="trending-tag"><a href="#">Angry Korea</a></div>
      <div class="trending-tag"><a href="#">Friendly Korea</a></div>
      <div class="trending-tag"><a href="#">Snapple</a></div>
      <div class="trending-tag"><a href="#">NAFTA SCHMAFTA</a></div>
      <div class="trending-tag"><a href="#">Superhero Film #11571</a></div>
      <div class="trending-tag"><a href="#">Wyoming Overrun With Hornets</a></div>
    </div>
  </div>
</div>
