<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>The Calgary Times</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Exo+2|Noto+Serif|Yanone+Kaffeesatz" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="/css/app.css">
    <script src="https://use.fontawesome.com/d41a0ab025.js"></script>
</head>
<body>
    @extends('layouts.app')
    @section('content')
    @include('header')
    @include('main')
    @endsection

</body>

</html>
