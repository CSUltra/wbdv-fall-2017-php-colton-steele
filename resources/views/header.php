<div class="topAndLogo">
  <div class="flex-container">
    <div>
      <h2 class="logo flex-1">The Calgary Times</h2>
    </div>
    <div>
      <input type="text" name="searchbar" value="" class="searchbar flex-2">
    </div>
  </div>
</div>

<div class="databar">
  <div class="flex-container">
    <div class="flex-container flex-1">
      <div class="databarPush">
        <a href="#" class="headercolor">Headlines</a>
      </div>
      <div class="databarAdjust">
        <a href="#" class="headercolor">Local</a>
      </div>
      <div class="databarAdjust">
        <a href="#" class="headercolor">For You</a>
      </div>
    </div>
  </div>
</div>
