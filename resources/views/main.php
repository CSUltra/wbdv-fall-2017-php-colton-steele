<main class="main flex-container">
    <!--since not a .blade file, use this method of include:-->
    <div class="sidebar flex-1">
        <?php include '../resources/views/sidebar.php' ?>
    </div>

    <div class="content flex-2">
        <?php include '../resources/views/main-center.php' ?>
    </div>

    <div class="flex-1">
        <?php include '../resources/views/main-right.php' ?>
    </div>

</div>
