<div class="sectionstitle">Sections</div>

<div class="padcategories"><a href="#" class="linkfix">
  <i class="fa fa-star" aria-hidden="true"></i> Top Stories</a>
</div>
<div class="padcategories"><a href="#" class="linkfix">
  <i class="fa fa-globe" aria-hidden="true"></i> World</a>
</div>
<div class="padcategories"><a href="#" class="linkfix">
  <i class="fa fa-rocket" aria-hidden="true"></i> Technology</a>
</div>
<div class="padcategories">
  <a href="#" class="linkfix"><i class="fa fa-line-chart" aria-hidden="true"></i> Business</a>
</div>
<div class="padcategories"><a href="#" class="linkfix">
  <i class="fa fa-heartbeat" aria-hidden="true"></i> Health</a>
</div>
<div class="padcategories"><a href="#" class="linkfix">
  <i class="fa fa-flask" aria-hidden="true"></i> Science</a>
</div>
<div class="padcategories"><a href="#" class="linkfix">
  <i class="fa fa-futbol-o" aria-hidden="true"></i> Sports</a>
</div>
<div class="padcategories"><a href="#" class="linkfix">
  <i class="fa fa-camera" aria-hidden="true"></i> Entertainment</a>
</div>
<div class="padcategories"><a href="#" class="linkfix">
  <i class="fa fa-handshake-o" aria-hidden="true"></i> Politics</a>
</div>
