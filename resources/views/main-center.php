<div class="main-title">Top Stories</div>


<ul>
  <!-- Loop for article format after each posting -->
  <?php foreach($articles as $article) { ?>
    <li>
      <div class="single-article-1">
        <div class="article-format"><?php echo $article->headline; ?></div>
      </div>
      <div class="single-article-2">
        <span class="source-font">Source:</span>
        <span class="source-name">
          <!-- $article->user->id to get id of a specific user, otherwise, it will default to
          the first user -->
          <a href="/user/<?php echo $article->user->id ?>"><?php echo $article->user->name ?></a>
        </span>
      </div>
    </li>
  <?php } ?>
</ul>

<!-- Post headline. CSRF_field to prevent unauthorized actions on behalf of a
user -->
<?php if(Auth::check()) { ?>
  <form class="" action="/entry" method="post">
    <?php echo csrf_field() ?>
    <div class="<?php echo $errors->has('headline') ? 'error' : '' ?>">
      <textarea name="headline" rows="8" cols="80" class="textarea-size">
      </textarea>
      <span><?php echo $errors->first('headline')?></span>
    </div>
    <input type="submit" name="submit" value="Post" class="submit-button">
  </form>
<?php } ?>
